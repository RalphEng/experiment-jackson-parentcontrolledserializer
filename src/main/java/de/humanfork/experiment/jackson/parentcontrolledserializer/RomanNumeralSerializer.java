package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson Serializer that serialize interger values with Roman Numerals.
 */
public class RomanNumeralSerializer extends StdSerializer<Integer> {
    
    private static final long serialVersionUID = -343281292889498295L;

    public RomanNumeralSerializer() {
        super(Integer.class);
    }

    @Override
    public void serialize(Integer value, JsonGenerator gen, SerializerProvider provider) throws IOException {
       
        if (value != null) {
            gen.writeString(RomanNumerals.toRomanNumerals(value));
        } else {
            gen.writeNull();
        }        
    }

}
