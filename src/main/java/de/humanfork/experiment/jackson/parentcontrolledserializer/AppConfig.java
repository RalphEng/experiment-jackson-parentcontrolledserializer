package de.humanfork.experiment.jackson.parentcontrolledserializer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class AppConfig {

    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        
        objectMapper.registerModule(new SimpleModule().setSerializerModifier(new SwitchingDetectBeanSerializerModifier()));
        objectMapper.registerModule(new SimpleModule().setSerializerModifier(new SwitchingExecuteBeanSerializerModifier()));
        
        return objectMapper;
    }
}
