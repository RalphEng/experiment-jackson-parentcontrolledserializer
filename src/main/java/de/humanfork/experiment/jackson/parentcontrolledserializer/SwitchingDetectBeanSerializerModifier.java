package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import de.humanfork.experiment.jackson.parentcontrolledserializer.SwitchingSerializer.SwitchToRoman;

public class SwitchingDetectBeanSerializerModifier extends BeanSerializerModifier {

    public static final String MARKER = "MARKER";

    @Override
    public JsonSerializer<?> modifySerializer(SerializationConfig config, BeanDescription beanDesc,
            final JsonSerializer<?> serializer) {
        
        if (beanDesc.getBeanClass().isAnnotationPresent(SwitchToRoman.class)) {
            //Add an delegating serializer that delegete to the normal serializer, but also enable the Marker flag (and restore the orignal marker afterwards)
            return new JsonSerializer<Object>() {
                @Override
                public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
                    
                    Object originalMarker = provider.getAttribute(MARKER);
                    provider.setAttribute(MARKER, true);
                    try {
                        ((JsonSerializer<Object>) serializer).serialize(value, gen, provider);
                    } finally {
                        provider.setAttribute(MARKER, originalMarker);
                    }
                }
            };
        } else {
            return serializer;
        }
    }

}
