package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanSerializer;

public class BeanSerializerDelegate extends JsonSerializer<Object> {

    private JsonSerializer<Object> delegate;
    
    
    public BeanSerializerDelegate(JsonSerializer<Object> delegate) {
        this.delegate = delegate;
    }

    @Override
    public void serialize(Object bean, JsonGenerator gen, SerializerProvider provider) throws IOException {
        provider.setAttribute(SwitchingDetectBeanSerializerModifier.MARKER, true);
        this.delegate.serialize(bean, gen, provider);        
    }
    
    
}
