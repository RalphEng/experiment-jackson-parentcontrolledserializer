package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.util.List;
import java.util.Optional;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import de.humanfork.experiment.jackson.parentcontrolledserializer.SwitchingSerializer.SwitchToRoman;

public class SwitchingSerializerTest {
    
    public static class ContainerA {
        public Demo demo;

        public ContainerA(Demo demo) {        
            this.demo = demo;
        }
    }
    
    @SwitchToRoman
    public static class ContainerB {
        public Demo demo;

        public ContainerB(Demo demo) {        
            this.demo = demo;
        }
    }
    
    public static class ContainerC {
        @SwitchToRoman
        public Demo demo;

        public ContainerC(Demo demo) {        
            this.demo = demo;
        }
    }
    
    public static class ContainerD {
        @SwitchToRoman
        public Optional<Demo> demo;

        public ContainerD(Demo demo) {        
            this.demo = Optional.of(demo);
        }
    }
    
    public static class ContainerE {
        @SwitchToRoman
        public List<Demo> demos;

        public ContainerE(Demo demo) {        
            this.demos = List.of(demo, demo);
        }
    }
    
    
    public static class ContainerF {
        @SwitchToRoman
        private Demo demo;

        public ContainerF(Demo demo) {        
            this.demo = demo;
        }
        
        public Demo getDemo() {
            return this.demo;
        }        
    }
    
    public static class ContainerG {
        private Demo demo;

        public ContainerG(Demo demo) {        
            this.demo = demo;
        }
        
        @SwitchToRoman
        public Demo getDemo() {
            return this.demo;
        }        
    }
    
    public static class Demo {

        @JsonSerialize(using = SwitchingSerializer.class)
        public int value;

        public Demo(int value) {
            this.value = value;
        }
    }
    
    @Test
    public void testPlain() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new Demo(1));

        // @formatter:off
        String expected = ( 
                "{              "
              +  "  'value' : 1 "
              + "}              ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void testContainerA() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new ContainerA(new Demo(1)));

        // @formatter:off
        String expected = ( 
                "{                 "
              +  " 'demo' : {      "
              +  "    'value' : 1  "
              +  " }               "
              + "}                 ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerB() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new ContainerB(new Demo(1)));

        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'demo' : {       "
              +  "    'value' : 'I' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerC() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new ContainerC(new Demo(1)));

        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'demo' : {       "
              +  "    'value' : 'I' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerD() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());

        String jsonResult = objectMapper.writeValueAsString(new ContainerD(new Demo(1)));
        
        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'demo' : {       "
              +  "    'value' : 'I' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerE() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());

        String jsonResult = objectMapper.writeValueAsString(new ContainerE(new Demo(1)));
        
        // @formatter:off
        String expected = ( 
                "{                   "
              +  " 'demos' : [       "
              + "    {               "
              +  "     'value' : 'I' "
              +  "   },              "
              + "    {               "
              +  "     'value' : 'I' "
              +  "   }               "
              +  "  ]                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerF() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new ContainerF(new Demo(1)));

        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'demo' : {       "
              +  "    'value' : 'I' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerG() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new ContainerG(new Demo(1)));

        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'demo' : {       "
              +  "    'value' : 'I' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
}
