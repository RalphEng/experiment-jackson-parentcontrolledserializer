package de.humanfork.experiment.jackson.parentcontrolledserializer;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import static java.util.Locale.ENGLISH;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Jackson Serializer that serialize integer values with Roman Numerals.
 * 
 * This Serializer serialize a integer with Roman Numerals if a parent container or field is annotated with {@link SwitchToRoman}.
 * 
 * example:
 *  
 *  given some class {@code Demo} with a field {@code value} that use this serializer
 *  <pre><code>
 *  public static class Demo {
 *
 *      {@literal @}JsonSerialize(using = SwitchingSerializer.class)
 *      public int value;
 *  }
 *  </pre></code>
 * 
 *  When this class {@code Demo} is used:
 *  <ul>
 *     <li>
 *          without: {@link SwitchToRoman} annotation (ContainerA),
 *          then the int value is serialized with normal int value
 *     </li>
 *     <li>
 *          with: {@link SwitchToRoman} annotation at parent container class (ContainerB),
 *          then the int value is serialized with Roman Numerals
 *     </li>
 *     <li>
 *          with: {@link SwitchToRoman} annotation at parent container field for {@code Demo} (ContainerC),
 *          then the int value is serialized with Roman Numerals
 *     </li>
 *  </ul>
 *  <pre><code>
 *  public static class ContainerA {
 *      public Demo demo;
 *  }
 *  </pre></code>
 *  
 *  <pre><code>
 *  {@literal @}SwitchToRoman
 *  public static class ContainerB {
 *      public Demo demo;
 *  }
 *  </pre></code>
 *  
 *  <pre><code>
 *  public static class ContainerC {
 *      {@literal @}SwitchToRoman
 *      public Demo demo;
 *  }
 *  </pre></code>
 *  
 *  This implementation scan for every {@link Integer} that is to be serialized for the {@link SwitchToRoman} annotation.
 *  At Field and Class level. If no controlling annotation is found, then it scann the parent context, that is the container
 *  class in the example above.
 */
public class SwitchingSerializer extends StdSerializer<Integer> {

    @Retention(RUNTIME)
    @Target({ TYPE, FIELD, METHOD })
    public @interface SwitchToRoman {

    }

    private static final long serialVersionUID = -343281292889498295L;

    private static final RomanNumeralSerializer ROMAN_NUMERAL_SERIALIZER = new RomanNumeralSerializer();

    public SwitchingSerializer() {
        super(Integer.class);
    }

    @Override
    public void serialize(Integer value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        if (hasSwitchToRomanAnnotation(gen.getOutputContext())) {
            ROMAN_NUMERAL_SERIALIZER.serialize(value, gen, provider);
        } else {
            provider.defaultSerializeValue(value, gen);
        }

    }

    boolean hasSwitchToRomanAnnotation(JsonStreamContext context) {
        Object currentValue = context.getCurrentValue();

        if (currentValue != null) {
            Class<? extends Object> currentClass = currentValue.getClass();

            if (currentClass.isAnnotationPresent(SwitchToRoman.class)) {
                return true;
            }

            if (context.getCurrentName() != null) {
                try {
                    //TODO scan super classes
                    if (currentClass.getDeclaredField(context.getCurrentName())
                            .isAnnotationPresent(SwitchToRoman.class)) {
                        return true;
                    }
                } catch (NoSuchFieldException e) {

                }

                try {
                    //GETTER are always public, so we do not need to check for declared fields in top level classes.
                    if (currentClass.getMethod(getterName(context.getCurrentName()))
                            .isAnnotationPresent(SwitchToRoman.class)) {
                        return true;
                    }
                } catch (NoSuchMethodException e) {
                }
            }
        }

        /* recursion to json parent */
        if (context.getParent() != null) {
            return hasSwitchToRomanAnnotation(context.getParent());
        }

        return false;
    }

    /**
     * Returns a String which capitalizes the first letter of the string.
     */
    public static String getterName(String fieldName) {
        Objects.nonNull(fieldName);

        return "get" + fieldName.substring(0, 1).toUpperCase(ENGLISH) + fieldName.substring(1);
    }

}
