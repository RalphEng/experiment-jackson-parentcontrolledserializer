package de.humanfork.experiment.jackson.parentcontrolledserializer.usecase;

import de.humanfork.experiment.jackson.parentcontrolledserializer.SwitchingSerializer.SwitchToRoman;

public class ContainerC {

    @SwitchToRoman
    private Inner inner;

    public ContainerC() {
        super();
    }

    public ContainerC(Inner inner) {
        this.inner = inner;
    }

    public Inner getInner() {
        return inner;
    }

    public void setInner(Inner inner) {
        this.inner = inner;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((inner == null) ? 0 : inner.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ContainerC other = (ContainerC) obj;
        if (inner == null) {
            if (other.inner != null)
                return false;
        } else if (!inner.equals(other.inner))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ContainerB [inner=" + inner + "]";
    }

}
