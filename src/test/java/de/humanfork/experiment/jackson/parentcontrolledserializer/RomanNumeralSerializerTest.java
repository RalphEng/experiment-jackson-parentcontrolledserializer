package de.humanfork.experiment.jackson.parentcontrolledserializer;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class RomanNumeralSerializerTest {
    public static class Demo {

        @JsonSerialize(using = RomanNumeralSerializer.class)
        public int value;

        public Demo(int value) {
            this.value = value;
        }

    }

    @Test
    public void testRomanNumeral() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = new ObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new Demo(3));

        // @formatter:off
        String expected = ( 
                "{                  "
              +  "  'value' : 'III' "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }
}
