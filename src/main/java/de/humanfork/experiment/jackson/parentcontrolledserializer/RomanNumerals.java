package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * Converter an integer to Roman numbers (String).
 */
public class RomanNumerals {

    private static LinkedHashMap<String, Integer> ROMAN_NUMERALS;
    static {
        ROMAN_NUMERALS = new LinkedHashMap<String, Integer>();
        ROMAN_NUMERALS.put("M", 1000);
        ROMAN_NUMERALS.put("CM", 900);
        ROMAN_NUMERALS.put("D", 500);
        ROMAN_NUMERALS.put("CD", 400);
        ROMAN_NUMERALS.put("C", 100);
        ROMAN_NUMERALS.put("XC", 90);
        ROMAN_NUMERALS.put("L", 50);
        ROMAN_NUMERALS.put("XL", 40);
        ROMAN_NUMERALS.put("X", 10);
        ROMAN_NUMERALS.put("IX", 9);
        ROMAN_NUMERALS.put("V", 5);
        ROMAN_NUMERALS.put("IV", 4);
        ROMAN_NUMERALS.put("I", 1);
    }

    public static String toRomanNumerals(int value) {
        StringBuilder result = new StringBuilder();

        int remaining = value;
        while (remaining > 0) {
            Entry<String, Integer> nextRoman = findGreatestMatch(remaining);
            result.append(nextRoman.getKey());
            remaining -= nextRoman.getValue();
        }
        return result.toString();
    }

    private static Entry<String, Integer> findGreatestMatch(int value) {
        return ROMAN_NUMERALS.entrySet().stream().filter(e -> e.getValue() <= value).findFirst().get();
    }

}
