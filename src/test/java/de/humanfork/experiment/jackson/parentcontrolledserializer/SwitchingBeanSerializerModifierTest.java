package de.humanfork.experiment.jackson.parentcontrolledserializer;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import de.humanfork.experiment.jackson.parentcontrolledserializer.SwitchingSerializer.SwitchToRoman;

public class SwitchingBeanSerializerModifierTest {

    public static class ContainerA {
        public Demo demo1;

        public Demo demo2;

        public ContainerA(Demo demo1, Demo demo2) {
            this.demo1 = demo1;
            this.demo2 = demo2;
        }
    }

    @SwitchToRoman
    public static class ContainerB {
        public Demo demo1;

        public Demo demo2;

        public ContainerB(Demo demo1, Demo demo2) {
            this.demo1 = demo1;
            this.demo2 = demo2;
        }
    }

    public static class ContainerABA {
        public ContainerA containerA1;
        public ContainerB containerB;
        public ContainerA containerA2;

        public ContainerABA(ContainerA containerA1, ContainerB containerB, ContainerA containerA2) {
            this.containerA1 = containerA1;
            this.containerB = containerB;
            this.containerA2 = containerA2;
        }

    }
    
    @SwitchToRoman
    public static class ContainerABAExplicite {
        public ContainerA containerA1;
        public ContainerB containerB;
        public ContainerA containerA2;

        public ContainerABAExplicite(ContainerA containerA1, ContainerB containerB, ContainerA containerA2) {
            this.containerA1 = containerA1;
            this.containerB = containerB;
            this.containerA2 = containerA2;
        }

    }

    public static class Demo {

        public int value;

        public Demo(int value) {
            this.value = value;
        }
    }

    private ObjectMapper initObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.registerModule(new SimpleModule() {
            @Override
            public void setupModule(SetupContext context) {
                super.setupModule(context);
                context.addBeanSerializerModifier(new SwitchingDetectBeanSerializerModifier());
                context.addBeanSerializerModifier(new SwitchingExecuteBeanSerializerModifier());

            }
        });
        return objectMapper;
    }

    @Test
    public void testPlain() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();

        String jsonResult = objectMapper.writeValueAsString(new Demo(1));

        // @formatter:off
        String expected = ( 
                "{              "
              +  "  'value' : 1 "
              + "}              ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void testContainerA() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();
        String jsonResult = objectMapper.writeValueAsString(new ContainerA(new Demo(1), new Demo(2)));

        JSONAssert.assertEquals(expectedResult(1, 2), jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void testContainerB() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();
        String jsonResult = objectMapper.writeValueAsString(new ContainerB(new Demo(1), new Demo(2)));

        JSONAssert.assertEquals(expectedResult("I", "II"), jsonResult, JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void testContainerMixed() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();

        JSONAssert.assertEquals(expectedResult(1, 11),
                objectMapper.writeValueAsString(new ContainerA(new Demo(1), new Demo(11))),
                JSONCompareMode.NON_EXTENSIBLE);
        JSONAssert.assertEquals(expectedResult("II", "XII"),
                objectMapper.writeValueAsString(new ContainerB(new Demo(2), new Demo(12))),
                JSONCompareMode.NON_EXTENSIBLE);
        JSONAssert.assertEquals(expectedResult(3, 13),
                objectMapper.writeValueAsString(new ContainerA(new Demo(3), new Demo(13))),
                JSONCompareMode.NON_EXTENSIBLE);
        JSONAssert.assertEquals(expectedResult("IV", "XIV"),
                objectMapper.writeValueAsString(new ContainerB(new Demo(4), new Demo(14))),
                JSONCompareMode.NON_EXTENSIBLE);
    }

    @Test
    public void testContainerABA() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();

        ContainerABA containerABA = new ContainerABA(new ContainerA(new Demo(1), new Demo(11)),
                new ContainerB(new Demo(2), new Demo(12)), new ContainerA(new Demo(3), new Demo(13)));

        // @formatter:off
        String expected = (
                  "{                                    "
                + "   'containerA1' : {                 "                 
                + "       'demo1' : {                   "
                + "           'value' : 1               "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 11              "
                + "       }                             "
                + "   },                                "
                + "   'containerB' : {                  "                 
                + "       'demo1' : {                   "
                + "           'value' : 'II'            "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 'XII'           "
                + "       }                             "
                + "   },                                "
                + "   'containerA2' : {                 "                 
                + "       'demo1' : {                   "
                + "           'value' : 3               "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 13              "
                + "       }                             "
                + "   }                                 "                
                + "}                                    ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, objectMapper.writeValueAsString(containerABA),
                JSONCompareMode.NON_EXTENSIBLE);
    }
    
    @Test
    public void testContainerABAExplicite() throws JsonProcessingException, JSONException {

        ObjectMapper objectMapper = initObjectMapper();

        ContainerABAExplicite containerABA = new ContainerABAExplicite(new ContainerA(new Demo(1), new Demo(11)),
                new ContainerB(new Demo(2), new Demo(12)), new ContainerA(new Demo(3), new Demo(13)));

        // @formatter:off
        String expected = (
                  "{                                    "
                + "   'containerA1' : {                 "                 
                + "       'demo1' : {                   "
                + "           'value' : 'I'             "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 'XI'            "
                + "       }                             "
                + "   },                                "
                + "   'containerB' : {                  "                 
                + "       'demo1' : {                   "
                + "           'value' : 'II'            "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 'XII'           "
                + "       }                             "
                + "   },                                "
                + "   'containerA2' : {                 "                 
                + "       'demo1' : {                   "
                + "           'value' : 'III'           "
                + "       },                            "
                + "       'demo2' : {                   "
                + "           'value' : 'XIII'          "
                + "       }                             "
                + "   }                                 "                
                + "}                                    ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, objectMapper.writeValueAsString(containerABA),
                JSONCompareMode.NON_EXTENSIBLE);

    }

    private static String expectedResult(String value1, String value2) {
        // @formatter:off
        return  ( 
                "{                           "
              +  " 'demo1' : {               "
              +  "    'value' : '"+value1+"' "
              +  " },                        "
              +  " 'demo2' : {               "
              +  "    'value' : '"+value2+"' "
              +  " }                         "
              + "}                           ").replaceAll("'", "\"");
        // @formatter:on
    }

    private static String expectedResult(int value1, int value2) {
        // @formatter:off
        return  ( 
                "{                          "
              +  " 'demo1' : {              "
              +  "    'value' : "+value1+"  "
              +  " },                       "
              +  " 'demo2' : {              "
              +  "    'value' : "+value2+"  "
              +  " }                        "
              + "}                          ").replaceAll("'", "\"");
        // @formatter:on
    }
}
