package de.humanfork.experiment.jackson.parentcontrolledserializer;

import org.json.JSONException;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.humanfork.experiment.jackson.parentcontrolledserializer.usecase.ContainerA;
import de.humanfork.experiment.jackson.parentcontrolledserializer.usecase.ContainerB;
import de.humanfork.experiment.jackson.parentcontrolledserializer.usecase.ContainerC;
import de.humanfork.experiment.jackson.parentcontrolledserializer.usecase.Inner;

/**
 * 
 * @author Ralph Engelmann
 */
public class AppTest {
    
    ObjectMapper objectMapper = new AppConfig().objectMapper();
    
    @Test
    public void testContainerA() throws JsonProcessingException, JSONException {
        ContainerA container = new ContainerA(new Inner(5));
        
        String jsonResult = objectMapper.writeValueAsString(container);
        
        // @formatter:off
        String expected = ( 
                "{                 "
              +  " 'inner' : {     "
              +  "    'value' : 5  "
              +  " }               "
              + "}                 ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);      
    }
    
    @Test
    public void testContainerB() throws JsonProcessingException, JSONException {
        ContainerB container = new ContainerB(new Inner(5));
        
        String jsonResult = objectMapper.writeValueAsString(container);
        
        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'inner' : {      "
              +  "    'value' : 'V' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);      
    }
    
    @Disabled("currently not supported")
    @Test
    public void testContainerC() throws JsonProcessingException, JSONException {
        ContainerC container = new ContainerC(new Inner(5));
        
        String jsonResult = objectMapper.writeValueAsString(container);
        
        // @formatter:off
        String expected = ( 
                "{                  "
              +  " 'inner' : {      "
              +  "    'value' : 'V' "
              +  " }                "
              + "}                  ").replaceAll("'", "\"");
        // @formatter:on
        JSONAssert.assertEquals(expected, jsonResult, JSONCompareMode.NON_EXTENSIBLE);      
    }
}
