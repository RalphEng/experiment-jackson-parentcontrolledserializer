Experiment: Jackson parent controlled serializer
================================================

This is an experiment with JSON mapper: [Jackson/FasterXML](https://github.com/FasterXML/jackson) (version 2.10.0).

I try to add an serializer that switches its behavior by an annotation in the parent object.
For example:

```
class Inner {
    
    @JsonSerializer(using = ParentContolledSwitchingSerializer.class)
    int value;
}


class ContainerA {

   @ParentContolled(strategy="A")
   private Inner inner;
}

class ContainerB {

   @ParentContolled(strategy="B")
   private Inner inner;
}
```


Interessing sources:
- https://stackoverflow.com/questions/34965201/customize-jackson-objectmapper-to-read-custom-annotation-and-mask-fields-annotat#
- https://stackoverflow.com/questions/7161638/how-do-i-use-a-custom-serializer-with-jackson
- https://www.baeldung.com/jackson-custom-serialization
- https://www.baeldung.com/jackson-call-default-serializer-from-custom-serializer
- https://www.baeldung.com/jackson-inheritance