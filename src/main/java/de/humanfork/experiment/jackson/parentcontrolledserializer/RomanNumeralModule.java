package de.humanfork.experiment.jackson.parentcontrolledserializer;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;

/**
 * Jackson Modul that register {@link RomanNumeralSerializer}.
 */
public class RomanNumeralModule extends SimpleModule {

    private static final long serialVersionUID = 2556423354954158181L;

    public RomanNumeralModule() {
        addSerializer(new RomanNumeralSerializer());        
    }
    
    @Override
    public String getModuleName() {
        return "RomanNumeralModule";
    }

    @Override
    public Version version() {
        return new Version(1, 0, 0, "SNAPSHOT",
                "de.humanfork.experiment.jackson.parentcontrolledserializer",
                "experiment-jackson-parentcontrolledserializer");
    }

}
