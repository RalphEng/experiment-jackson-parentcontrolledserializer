package de.humanfork.experiment.jackson.parentcontrolledserializer;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;

public class SwitchingExecuteBeanSerializerModifier extends BeanSerializerModifier {

    public static final String MARKER = SwitchingDetectBeanSerializerModifier.MARKER;

    @Override
    public JsonSerializer<?> modifySerializer(SerializationConfig config, BeanDescription beanDesc,
            final JsonSerializer<?> serializer) {

        //        Boolean romanNumberMarker = (Boolean) provider.getAttribute(SwitchingDetectBeanSerializerModifier.MARKER);
        //
        //        if (romanNumberMarker != null && romanNumberMarker) {
        //            new RomanNumeralSerializer().serialize(value, gen, provider);
        //        } else {
        //            provider.defaultSerializeValue(value, gen);
        //        }        

        return new JsonSerializer<Object>() {
            @Override
            public void serialize(Object value, JsonGenerator gen, SerializerProvider provider) throws IOException {
                
                Boolean romanNumberMarker = (Boolean) provider.getAttribute(SwitchingDetectBeanSerializerModifier.MARKER);
                
                if (romanNumberMarker != null && romanNumberMarker && value instanceof Integer) {
                    new RomanNumeralSerializer().serialize((Integer)value, gen, provider);
                } else {
                    ((JsonSerializer<Object>) serializer).serialize(value, gen, provider);
                }
            }
        };
    }

}
