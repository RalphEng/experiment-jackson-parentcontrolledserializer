package de.humanfork.experiment.jackson.parentcontrolledserializer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class RomanNumeralsTest {

    @CsvSource({ "1, I", "2, II", "3, III", "4, IV", "5, V", "6, VI", "7, VII", "8, VIII", "9, IX", "10, X" })
    @ParameterizedTest
    public void testToRomanNumeral(int value, String expectedRoman) {
        String roman = RomanNumerals.toRomanNumerals(value);
        assertEquals(expectedRoman, roman);
    }

}
